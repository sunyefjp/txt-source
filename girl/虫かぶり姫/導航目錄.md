# CONTENTS

虫かぶり姫  
書蟲公主  

作者： 由唯  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%99%AB%E3%81%8B%E3%81%B6%E3%82%8A%E5%A7%AB.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/girl/%E6%9B%B8%E8%9F%B2%E5%85%AC%E4%B8%BB.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/girl/out/%E6%9B%B8%E8%9F%B2%E5%85%AC%E4%B8%BB.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/girl/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/girl/虫かぶり姫/導航目錄.md "導航目錄")




## [徒有虛名的婚約者篇](00000_%E5%BE%92%E6%9C%89%E8%99%9B%E5%90%8D%E7%9A%84%E5%A9%9A%E7%B4%84%E8%80%85%E7%AF%87)

- [序幕─１](00000_%E5%BE%92%E6%9C%89%E8%99%9B%E5%90%8D%E7%9A%84%E5%A9%9A%E7%B4%84%E8%80%85%E7%AF%87/00010_%E5%BA%8F%E5%B9%95%E2%94%80%EF%BC%91.txt)

