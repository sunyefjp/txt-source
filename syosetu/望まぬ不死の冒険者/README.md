# novel

- title: 望まぬ不死の冒険者
- title_zh: 事與願違的不死冒險者
- title_zh2: 非自願的不死冒險者
- title_zh3: 意外成為不死族的冒險者
- author: 丘/丘野　優
- source: http://ncode.syosetu.com/n8577dn/
- cover: https://images-na.ssl-images-amazon.com/images/I/91HFHrtpF0L.jpg
- publisher: syosetu
- date: 2018-09-09T18:00:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- じゃいあん
- 中曽根ハイジ

## series

- name: 望まぬ不死の冒険者

## preface


```
以最頂級的神銀級冒險者為目標，轉眼過了十年。仍是區區銅級冒險者的雷特，獨自闖進《水月迷宮》，並在那裡碰到了「巨龍」，結果在壓倒性的力量面前，雷特束手無策地遭對手吞噬。

──就這樣，雷特「甦醒了」。

然而不知為何，他卻變成了最弱魔物「骨人」的模樣……！？

雷特為了避免遭人類剷除，決定將希望寄託在魔物的「存在進化」上－－也就是打倒其他魔物累積經驗，以進化成高階魔物為目標。因為如果能藉由存在進化變成「食屍鬼」，或許就能偽裝成人類。

而在這個過程中，雷特巧遇了人類的菜鳥冒險者莉娜。為拯救莉娜而擊敗魔物的雷特，也在同時體會到自己藉由存在進化所得到的力量……！？

與強大魔物戰鬥，目睹各種神祕事物，並且讓自己變得更加強大。
在死後仍持續朝遙遠的神銀級邁進，不死者雷特的「冒險」就此揭開序幕！

辺境で万年銅級冒険者をしていた主人公、レント。彼は運悪く、迷宮の奥で強大な魔物に出会い、敗北し、そして気づくと骨人《スケルトン》になっていた。このままで街にすら入れないと苦悩した結果、彼は魔物の存在進化の事を思い出し、とりあえず肉のある体を手に入れることを目指して、迷宮で魔物を倒していく。
```

## tags

- node-novel
- R15
- syosetu
- ファンタジー
- ハイファンタジー
- 残酷な描写あり
- 現地主人公
- 異世界
- 進化
- 魔物

# contribute

- nice天夜月
- 土之女神
- 做梦的快乐
- cyfung1031
- 水魚醬
- 草库金会
- celestia173
- 禾缗
- 啊呀呀
- 伪髅
- 龘實
- SDK_C
- 眼前的飞碟
- f2008zhq
- Alice
- wahaha9990920
- joseph261059
- 长衫造纸笼
- billy7645
- 碱式盐
- 碱暂停接坑
- SAN-略略
- ghdhdit
- 有棲七曜
- 

# options

## syosetu

- txtdownload_id: 918486

## textlayout

- allow_lf2: false

# link

- [dip.jp](https://narou.dip.jp/search.php?text=n8577dn&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [事与愿违的不死冒险者吧](https://tieba.baidu.com/f?kw=%E4%BA%8B%E4%B8%8E%E6%84%BF%E8%BF%9D%E7%9A%84%E4%B8%8D%E6%AD%BB%E5%86%92%E9%99%A9%E8%80%85&ie=utf-8 "事与愿违的不死冒险者")
- [txr by SDK_C](https://tieba.baidu.com/p/5854693784?pid=121650524888&cid=0&red_tag=0833195265#121650524888)
- [txt by 龘實](https://pan.baidu.com/s/1dI8KPYu1TdWqRKEUW-tuEg) x1z8 , [tieba](http://tieba.baidu.com/p/5363881548?pid=118578420733#118578420733)
- [txt by f2008zhq](https://pan.baidu.com/s/1IERTh8IYNkjPjSsgi3k4vQ) xc8g , [tieba](https://tieba.baidu.com/p/5967895104)
- [Alice txt](https://pan.baidu.com/s/1Uo-opKhJNwdpRRVPhLwn-Q) 4bzy
- [dm5](http://www.dm5.com/manhua-shiyuyuanweidebusimaoxianzhe/)
- 

