(好了，那就開始吧……)


和瓦沙克的召喚魔物戰鬥過後的第二天晚上——
塔瑪悄咪咪地從家裡溜出來，來到了迷宮裡。

而溜出來的理由是要積攢「經驗值」。
換句話說就是……還是直接看看塔瑪的狀態面板會更加容易理解。


＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
名字：塔瑪
種族：貝希摩斯(幼體)
固有技能：《屬性咆哮》《技能喰奪》《屬性劍尾》《獅子王的加護》《屬性彈》
喰奪技能：《收納》《劇毒獠牙》《飛翔》《火球》《冰槍》《鋼軀》《觸手召喚》《粘液無限射出》《異種交配》《龍之獠牙》《龍爪》
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝


技能欄並沒有什麼變化，但是進化的選項又和上次一樣消失不見了。

(果然，這是和雙頭犬獨眼巨人戰鬥那時用掉了的原因吧……。不過關於這個進化選項出現的機制，心裡也有點底了吶)


之前通過不斷地戰鬥，塔瑪便可以選擇進化成貝希摩斯的第二形態。而那之後，又通過擊敗死靈龍讓進化成貝希摩斯第三形態的選項顯現了出來……。


戰鬥技巧提升應該是一個重要因素，但可能還需要與魔物進行戰鬥並以此獲取經驗，而這個經驗值達到一定數值的時候才會顯示進化的選項……？塔瑪從之前的經歷中想到了這個可能性。


因此，塔瑪在深夜時從艾麗婭的床上溜出來，特意到迷宮裡積攢經驗值。


『嘎嘎！』
『咕嘎咕嘎！』


塔瑪一走進迷宮裡，兩隻哥布林便出現了。


「喵(《炎刃》！)」


塔瑪隨即發動了《屬性劍尾》的其中一種，將它們貫穿了。
而果不其然，兩隻哥布林都被瞬殺了。

(反正今天主人她們不在，決戰也近在眼前了，就讓這個迷宮裡的魔物都成為吾輩進化的經驗值吧……！)

塔瑪可愛的眼瞳變尖細了起來，往迷宮的深處前進了。




◆




『嘎叭——……！』


迷宮第十五層——


塔瑪又一次來到了之前激戰過的階層，他的面前出現了一隻魔物。


巨魔——


那是艾麗婭她們曾經聯手擊敗的A級魔物。


「喵啊(哼，就攢經驗來說這樣的剛好吶……看招)！」


一看到巨魔，塔瑪就發出了可愛的咆哮聲。
他發動了《屬性彈》其一的《水導彈》。


藍色的導彈向著巨魔飛去，直擊了後者的腹部。
接著下一瞬，巨魔被水做的牢籠囚禁起來了。


掙扎不已的巨魔……還真不愧是A級魔物，它硬是憑借自身壓倒性的力量掙扎，就快要從水牢裡出來了。


「喵嗯(《水咆哮》)……！」


然而，塔瑪可不會眼睜睜看著敵人逃脫。
他向著巨魔從水牢裡伸出來的手臂打了一發《屬性咆哮》。


感到劇痛的巨魔抵抗的力氣減弱了一分，而且由於疼到想要大喊卻無法作聲，反而因此喝了一大口水。


巨魔有著再生能力，它的手腕已經恢復如常，不過……它掙扎的力度卻越來越弱了。在沒法呼吸的情況下還嗆了水，會這樣也是理所當然的。


於是不久後……巨魔翻著白眼，在水牢裡咽氣了。


(好了，差不多可以再確認一下狀態了)

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
名字：塔瑪
種族：貝希摩斯(幼體)
固有技能：《屬性咆哮》《技能喰奪》《屬性劍尾》《獅子王的加護》《屬性彈》
喰奪技能：《收納》《劇毒獠牙》《飛翔》《火球》《冰槍》《鋼軀》《觸手召喚》《粘液無限射出》《異種交配》《龍之獠牙》《龍爪》
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

(唔……幹掉了巨魔也沒有進化的文字嗎……。傷腦筋吶，再狩獵多一會看看吧)


稍微變得沮喪起來的塔瑪往下一個階層前進了。


◆




『咕嗷——！』

迷宮第二十層——


在被岩石包圍著的空間裡，響起了一聲連大氣都被震動了的咆哮聲。


(嚯，「次龍」嗎。有點意思，這傢伙的話應該就能一口氣存不少經驗值了吧！)


在前方等待著塔瑪的魔物……。


它被稱作次龍，是一種下級的龍族魔物。


其全身覆蓋著堅硬的鱗片，用四隻腳走路，雖然不能進行吐息攻擊，但行動相當敏捷迅速。
儘管沒有S級的地龍那麼龐大的身軀，也有著五米左右的體長。
攻擊力方面比起上位龍族要差，卻也在巨魔之上。
等級定位在A到S之間，一般被認作A+級。


次龍一看到塔瑪，二話不說就把自己的長尾甩了過去。


「喵(想得美)！」


憑著騎士的經驗，加之貝希摩斯的動態視力，塔瑪立馬看穿了次龍的甩尾攻擊，原地起跳躲開了。


接著就那樣——

「喵嗯(《飛翔》)！」


——使用了以前在雙足飛龍身上得到的技能《飛翔》，一下子飛到了次龍的頭頂上方。


『咕嚕！？』


看上去弱如螻蟻的小動物不僅避開了自己的攻擊，還突然長出翅膀飛了起來……。
這個意料之外的情況，讓次龍不禁發出了驚愕的叫聲。

「喵啊~(吃我一招，《岩石咆哮》)！」


在次龍的頭頂正上方，塔瑪放出了地屬性的咆哮攻擊。

跟體格這麼巨大的敵人正面互剛還是免了……總而言之，就是要在對面打不到自己的位置瘋狂輸出就對了。


《岩石咆哮》產生的石塊不斷從次龍的頭上往下砸去，而面對這隻能挨打的憋屈場面，次龍顯露出了想要逃跑的樣子……。


(別想跑！)


……見此，塔瑪繼續往上飛，直到距離天花板只剩一點點距離時，再次釋放《岩石咆哮》。

不僅攻擊的起始高度更高，這一次的地屬性攻擊的勢頭還更加猛烈。
被無數的攻擊打中頭部，次龍最終還是禁受不住失去了意識，接著「嗙轟！」地倒在了地上。


(呼呣，暈過去了吶。那就由吾輩給你最後一擊吧)


朝著失去了意識的次龍，塔瑪輕鬆地靠近過去並發動《屬性劍尾》其一的《空刃》，接著就那樣……鏘——！地一聲，將次龍的頭顱貫穿了。

完事後，塔瑪再次確認其自己的狀態面板。


＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
名字：塔瑪
種族：貝希摩斯(幼體)
固有技能：《屬性咆哮》《技能喰奪》《屬性劍尾》《獅子王的加護》《屬性彈》
喰奪技能：《收納》《劇毒獠牙》《飛翔》《火球》《冰槍》《鋼軀》《觸手召喚》《粘液無限射出》《異種交配》《龍之獠牙》《龍爪》
進化可能：▷貝希摩斯第二形態
　　　　　▷貝希摩斯第三形態
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝


(嚯，進化選項出現了兩個！單純注重破壞力的第二形態，還有兼具神聖屬性和防禦力的第三形態嗎……。不過要是在主人她們面前用了第二形態的話，吾輩是貝希摩斯的事就會敗露了吧，那選項其實也就只有一個了吶……)

看到進化選項有兩個時滿心歡喜的塔瑪，一想到其實也就只能選其中一個之後感動有點掃興。


但是不管怎樣，目的也達成了。


塔瑪清洗完因為戰鬥而變臟起來的身體，然後——

「喵~~~~~~啊——！」


——用可愛的聲音發出了勝利的嚎叫。
