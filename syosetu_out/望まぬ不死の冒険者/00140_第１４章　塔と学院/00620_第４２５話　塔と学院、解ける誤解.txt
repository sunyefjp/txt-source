「⋯⋯我明白了所以不需要擔心。總而言之兩個人都先進來」

羅蕾露露出稍微驚訝的樣子一邊將我們帶進自己的房間。
奧格利立刻收回了慌張的表情，將肩上的兩人也一起帶了進來。
我好好確認了奧格利身後沒有任何人在，然後進入了羅蕾露的房間並仔細將門給關上了。


◆◇◆◇◆

「果然、是一樣的嗎？」

將肩上扛著的行李（兩人）放在床上 奧格利說道

在哪裡的是和現在帶來的兩人不一樣的、已經有一個人已經在這裡了。
和我跟奧格利的方法不同，這個人被魔術給綁住了，手腳都被光環給束縛住。
是有著相當程度的魔術吧。
即使竭盡全力、不管是我還是奧格利都沒有辦法能做到、

想要將魔術給繼續下去、會根據離施術者的距離以及經過的時間而變得困難。
如果不是使用什麼魔法道具的話，只使用術式就想要維持這樣的魔術是十分困難的一件事。

順便一提，被綁住的是一個男人。
是一個不管誰看都會覺得是個標準的美男子，絶對不會在這樣的村莊裡能看見的類型。

畢竟在這樣的村莊裡面，不管男女只要長得好看的話，大多會在在小的時候就被人帶進城市裡面。
並不是作為奴隷、而是作為商店的實習生、之後大多會成為某人的養子。
雖說如果是被帶到了什麼治安不好的地方的確是有可能變成奴隷、不過、在雅蘭的話奴隷是違法的。
沒人會敢明目張膽的做出這種事情。

雖然是這麼說，但是這樣的事情也並非完全不存在、這隻不過是人類的陰暗面罷了。
無藥可救的一群人。

在跟著騎士團和自治團努力的玩著捉迷藏。

「就是這麼回事。所以說奧格利扛著這樣一個可憐的女性，以及一個戴著可疑面具的人在前面帶路，只會讓人覺得是不知道從哪裡來的奴隷組織的一員將年輕的女性弄暈想帶走啊。再說了、就算是想販賣奴隷，在雅蘭也很困難的吧、根本賺不到錢。倒不如說比起做這種事，當一名普通的冒険者更能安全快捷的賺到錢才對吧。」

是什麼微妙的信任嗎、這以我和奧格利要是有什麼合理的理由的話就可能會去從事奴隷行業為前提的台詞。

⋯⋯不管怎樣都是在開玩笑吧。

「這男的⋯⋯？美人計⋯⋯？」

奧格利並沒有特別往那邊靠近、向羅蕾露詢問著事情的經過。

「啊⋯⋯好像是有想說什麼話的樣子，總之先放進來了，雖說是先稍微認真的講了會兒、但是突然想對我幹什麼、總而言之是先用土矢把他打昏了。」
「又有危險啊⋯⋯那個、直接被打中的話相當痛的誒。」

奧格利像是顫抖了一樣說到。
土矢只是將土塊給打出去、結構簡單而又單純的魔術而已，不過也正是這樣，才會更加反映出魔術師的能力。
像羅蕾露這樣的魔術如果認真釋放的話將幾枚鐵板打穿都很容易。
將這樣的⋯⋯對著只是一個村民⋯⋯？

「喂⋯這傢伙應該還活著吧⋯⋯？」

我終於問出了這個問題。

「那是當然的吧。就算是我也知道不管有什麼樣的理由在這樣的小的村子裡也不能突然殺人的吧。你們不也是一樣的嗎？」
「嘛⋯⋯」
「的確是這樣呢～」

明明譴責了別人，不過這的確是當然的吧。
再加上，還有其它的原因⋯⋯⋯

羅蕾露將靜音魔術給釋放出來、然後直接進入正題。

「所以、你們覺得這傢伙是《塞壬》本人還是她的同夥呢。」

雖然床上的三個人的確是暈過去了，不過還是為了以防萬一還是將靜音結界設置了不會讓他們聽到的範圍。
我安心的回答了她的問題。

「我們現在也有些疑惑啊。只是三個人⋯⋯這之後該怎麼辦呢。說不定有其他的可能性存在。而且⋯⋯有沒有感到什麼⋯奇怪的感覺？」
「⋯⋯的確是這樣啊。就像是、突然變了一個人一樣襲擊過來。這樣說是演技的話也太⋯⋯」

羅蕾露一邊摩擦這下巴一邊講道。
這之後奧格利也說話了。

「來找我的是菲麗絲就是了⋯⋯還記得嗎？菲麗絲。」
「啊啊。酒館給我們介紹的少女是吧？」
「對對，就是那個孩子。果然樣子很奇怪啊。雖然我並沒有馬上讓她昏過去、她就像是被抓到的野獸一樣大鬧起來。就像是丟了魂一樣。根本看不出來那是通過自己的意識在行動。」

也就是說，是這樣吧。
比起我跟羅蕾露直接的將她們打昏過去，注意到了什麼奇怪的地方吧。

而且，總覺得她們來找我們的時候感受不到任何自己的思想。
就像是服從這什麼命令而行動的人偶一樣。
就是這樣的感覺。
但是、現在要下定論的話還是太早了點、有必要再去確認一下。

「並沒有被施加了魔術的感覺。但是根據發生的事情來看，她們被誰操控的可能性十分的高。必須要去確認一下⋯⋯這樣的可能性。要叫醒一個人嗎？」
「要是又胡鬧起來該怎麼辦？」

奧格利縮著肩膀說到。

「那就沒辦法了。要是這樣的話、既然不是魔術，那讓他們恢復正常的方法我也不知道。只能再讓她們暈過去或者用魔術讓她們睡過去。只能早一點將《塞壬》本人給抓到了吧。」

的確只能這樣了。

「這樣的話⋯⋯總而言之現裝成被騙到的樣子比較好吧。要是注意到了的話想逃走的話會很難辦。而且，這個村莊的樣子也去看一看比較好吧。除了菲麗絲她們以外，有著同樣狀態的村民也說不定。」
「⋯⋯要是每個村民都是這樣該怎麼辦？」

奧格利說著不吉利的發言。
羅蕾露點了點了頭、

「原來如此《準備舞台》嗎。看來、每個村民都是這齣戲劇的演員嗎。」

我也同意這個可怕的想法。